<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/app.css" rel="stylesheet">

  <title>Sandro Schweizer</title>

</head>
<body>
<div class="columns">
  <div class="column is-8 is-offset-2">
    @yield('content')
  </div>
</div>
</body>
</html>
