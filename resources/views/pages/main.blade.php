@extends('app')
@section('content')
  <header>
    <div class="columns is-vertical">
      <div class="column is-6 is-offset-3 is-offset-1-mobile is-10-mobile">
        <h1 id="title">
          Welcome
        </h1>
      </div>
      <div class="column is-12 is-10-widescreen is-offset-1-widescreen is-10-mobile is-offset-1-mobile">
        <img id="main-image" src="http://placehold.it/1080x720">
      </div>
      <div class="columns is-mobile" id="icon-wrapper">
        <div class="column is-3 has-text-centered">
          <a href="https://twitter.com/SandroSchweizer" title="Twitter">
            <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
          </a>
        </div>
        <div class="column is-3 has-text-centered">
          <a href="https://github.com/raydirty" title="Github">
            <i class="fa fa-2x fa-github-square" aria-hidden="true"></i>
          </a>
        </div>
        <div class="column is-3 has-text-centered">
          <a href="https://bitbucket.org/santrick/" title="Bitbucket">
            <i class="fa fa-2x fa-bitbucket-square" aria-hidden="true"></i>
          </a>
        </div>
        <div class="column is-3 has-text-centered">
          <a href="https://www.linkedin.com/in/sandro-schweizer-96184211b/" title="LinkedIn">
            <i class="fa fa-2x  fa-linkedin-square" aria-hidden="true"></i>
          </a>
        </div>
      </div>
    </div>
  </header>
  <section>
    <div class="columns is-mobile is-vertical is-gapless">
      <div class="column is-6 is-offset-3" id="biography">
        <h2>
          Biography
        </h2>
        <hr class="bio">
      </div>
      <div class="column is-10 is-offset-1">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At commodi deserunt esse harum impedit ipsam,
          obcaecati, omnis perferendis porro sint ullam voluptates? Aperiam deleniti eveniet excepturi explicabo fugiat
          impedit ut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam beatae, consequuntur, corporis
          cumque ducimus facilis minima modi nobis quis sunt tempore tenetur totam veritatis. Accusantium at laborum
          praesentium rem sequi?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio expedita fuga harum
          iusto maiores minima perspiciatis, possimus rem suscipit tempore? Asperiores autem corporis eaque qui
          quibusdam quos recusandae saepe vero!</p>
      </div>
    </div>
  </section>
  <footer>
    <div class="columns is-mobile">
      <div class="column is-7 is-8-desktop is-offset-1">
        &copy; Sandro Schweizer, {{ date('Y') }}
      </div>
      <div class="column is-3 is-4-desktop">
        <div class="is-pulled-right">
          <a href="mailto:sandro.schweizer@santrick.com">Contact</a>
        </div>
      </div>
    </div>

  </footer>
@endsection